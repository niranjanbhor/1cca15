package ClassTypeCasting;

public class AirAsia extends Goibibo {

    double[] cost={3000.56,5000.56,8000.67};

    @Override
    void bookTicket(int qty, int routeChoice) {

        boolean found=false;
        for (int i=0;i<routes.length;i++)
        {
            if (routeChoice==i)
            {
                double total=qty*cost[i];
                System.out.println("TotalAmt:"+total);

                found=true;
            }
        }
        if (!found)
        {
            System.out.println("Invalid choice");
        }
    }
}
