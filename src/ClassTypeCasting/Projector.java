package ClassTypeCasting;

public class Projector extends Machine{
    @Override
    void getType() {
        System.out.println("Machine is projector");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("finalAmt:"+finalAmt);
    }
}
