package ClassTypeCasting;

public class Indigo extends Goibibo{

    double[] cost={4000.67,8000.67,12000.67};

    @Override
    void bookTicket(int qty, int routeChoice) {

        boolean found=false;
        for (int i=0;i<routes.length;i++)
        {
            if (routeChoice==i)
            {
                double total=qty*cost[i];
                System.out.println("TotalAmt:"+ total);
                found=true;
            }
        }
        if (!found)
        {
            System.out.println("Invalid choice");
        }
    }
}
