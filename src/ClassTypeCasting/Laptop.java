package ClassTypeCasting;

public class Laptop extends Machine {
    @Override
    void getType() {
        System.out.println("Machine Type Display");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double finalAmt=total+total*0.15;
        System.out.println("FinalTotal:"+finalAmt);
    }
}
