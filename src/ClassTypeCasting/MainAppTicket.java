package ClassTypeCasting;

import java.util.Scanner;

public class MainAppTicket {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select the service provider");
        System.out.println("1:AirAsia\n2:Indigo");
        int choice= sc1.nextInt();

        System.out.println("select rout");
        System.out.println("0:Mumbai-pune");
        System.out.println("1:Nagar-pune");
        System.out.println("2:Nashik Mumbai");
        int routeChoice= sc1.nextInt();
        System.out.println();

        System.out.println("Enter the Number of ticket");
        int ticket= sc1.nextInt();

        Goibibo g1=null;
        if (choice==1)
        {
            g1=new AirAsia();
        }
        else
        {
            g1=new Indigo();
        }
        g1.bookTicket(ticket,routeChoice);


    }
}
