package ClassTypeCasting;

import java.util.Scanner;

public class MainAppMachine {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the Qty");
        int qty=sc1.nextInt();
        System.out.println("Enter the price");
        double price= sc1.nextDouble();

        System.out.println("choice the machine type");
        System.out.println("1:Laptop\t2:Projector");
        int choice=sc1.nextInt();

        /*
         Projector p1=new Projector();
         if (choice==1)
         {
             p1.getType();
            p1.calculateBill(qty,price);
         }
         else
         {
             p1.getType();
             p1.calculateBill(qty,price);
         }
       */

        Machine m1=null;
        if (choice==1)
        {
            m1=new Laptop();     // Upcasting
        }
        else
        {
            m1=new Projector();  // Upcasting

        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
}
