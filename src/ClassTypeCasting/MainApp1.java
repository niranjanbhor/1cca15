package ClassTypeCasting;

public class MainApp1 {
    public static void main(String[] args) {
        Sample s1=new Sample();
        s1.test();
        s1.info();

        Demo d1=new Sample();  // Upcasting
        d1.test();
        //d1.info()                        Error because subclass method can not access in casting
       // ((Sample) d1).info();
    }
}
