package ClassTypeCasting;

public class MainAppTv {
    public static void main(String[] args) {
        LCD l1=new LCD();
        l1.display();
        l1.screenQuality();

        LED l2=new LED();
        l2.display();
        l2.screenQuality();

        Tv v1=new Tv();
        v1.display();


        System.out.println("==============================Upcasting=======================================");

        Tv t1;
        t1=new LCD();    // upcasting
        t1.display();    // the casting subclass into superclass is as upcasting

        t1=new LED();      // object is subclass and ReFrance is the superclass
        t1.display();     // upcasting

    }
}
