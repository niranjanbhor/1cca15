package Demo;

public class MainApp4 {
    static
    {
        System.out.println("Static Block");
    }

    {
        System.out.println("Non static block");
    }

    public static void main(String[] args) {
        System.out.println("Main Method");
        MainApp4 m1=new MainApp4();
        MainApp4 m2=new MainApp4();
        MainApp4 m3=new MainApp4();
       // MainApp4 m4=new MainApp4();
        //MainApp4 m5=new MainApp4();
    }
}
