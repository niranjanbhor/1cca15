package Demo;

public class Constructor
{
    static
    {
        System.out.println("Static Block");
    }

    public static void main(String[] args)
    {
        System.out.println("Main Method");
        Constructor b1=new Constructor();
        Constructor b2=new Constructor();
    }
    {
        System.out.println("Non static Block");
    }

}

