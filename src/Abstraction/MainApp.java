package Abstraction;

public class MainApp {
    public static void main(String[] args) {
        Switch s;
        s=new LedLight();
        s.switchOn();
        s.switchOff();

        s=new DlfLight();
        s.switchOn();
        s.switchOff();
    }
}
