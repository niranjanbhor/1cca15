package Abstraction;

public interface CarCompany {
    void byCar(int qty);
    void  financeCar(int qty);
    void  availableCar();
}
