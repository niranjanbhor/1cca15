package Abstraction;
//Step- 2
public class LoanAccount implements Account {
    private double loanAmount;
    //Account Creation
    public LoanAccount(double loanAmount) {
        this.loanAmount = loanAmount;
        System.out.println("Loan Account Created");
    }
    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs is Debited form LoanAccount");
    }

    @Override
    public void withDraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+"Rs Credit to Your Account");
    }

    @Override
    public void checkBal() {
        System.out.println("Active your lonaBalance"+loanAmount);
    }
}
