package Abstraction;
// Step -1
public class SavingAccount implements Account{

    private double accountBalance;
    // Account Creation
    public SavingAccount(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public void deposit(double amt) {
      accountBalance+=amt;
        System.out.println(amt+"Rs Credit from your Account");
    }

    @Override
    public void withDraw(double amt) {
        if (amt <= accountBalance) {
            accountBalance -= amt;
            System.out.println(amt+"Rs is WithDraw from your Account");
        }
        else
        {
            System.out.println("InSufficient Balance");
        }
    }

    @Override
    public void checkBal() {
        System.out.println("Active Balance"+accountBalance);
    }
}
