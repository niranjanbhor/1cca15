package Abstraction;

public class DlfLight implements Switch{
    @Override
    public void switchOn() {
        System.out.println("DlfLight Switch on ");
    }

    @Override
    public void switchOff() {
        System.out.println("DlfLight Switch is Off");

    }
}
