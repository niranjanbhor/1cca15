package Abstraction;

public class CompanyFactory {

    CarCompany openingShowroom(int type,int qty)

    {
        CarCompany c1=null;
        if (type==1)
        {
            c1=new PuneShowroom(qty);
        }
        else if (type==2)
        {
            c1=new LossShowroom(qty);
        }
        return c1;
    }
}
