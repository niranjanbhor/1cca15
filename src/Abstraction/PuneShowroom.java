package Abstraction;

public class PuneShowroom implements CarCompany{
    int byCarStock;
    //Create Stock

    public PuneShowroom(int byCarStock) {
        this.byCarStock = byCarStock;
    }

    @Override
    public void byCar(int qty) {
        byCarStock+=qty;
        System.out.println(qty+"Add The Car in Showroom");
    }

    @Override
    public void financeCar(int qty) {
       if (byCarStock<=qty)
       {
           byCarStock-=qty;
           System.out.println(qty+"Add or financeCar in the showroom ");
       }
       else
       {
           System.out.println("No Available in Car");
       }
    }

    @Override
    public void availableCar() {
        System.out.println("Available Car:"+byCarStock);

    }
}
