package Abstraction;

public class LossShowroom implements CarCompany{
    int lossCar;

    public LossShowroom(int lossCar) {
        this.lossCar = lossCar;
    }

    @Override
    public void byCar(int qty) {
        lossCar-=qty;
        System.out.println(qty+"Recover the car loss your Showroom");
    }

    @Override
    public void financeCar(int qty) {
        lossCar+=qty;
        System.out.println(qty+"add loss of the in your showroom");
    }

    @Override
    public void availableCar() {
        System.out.println("Total No of car loss"+lossCar);
    }
}
