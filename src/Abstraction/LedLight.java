package Abstraction;

public class LedLight implements Switch{
    @Override
    public void switchOn() {
        System.out.println("LedLight Switch is on");
    }

    @Override
    public void switchOff() {
        System.out.println("LedLight Switch is off");
    }
}
