package FORloop;

import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select language");
        System.out.println("1:java\n2:Python\n3:php");
        int choice=sc1.nextInt();

        switch (choice) {
            case 1 -> System.out.println("Select java");
            case 2 -> System.out.println("select Python");
            case 3 -> System.out.println("Select php");
            default -> System.out.println("invalid");
        }
    }
}
