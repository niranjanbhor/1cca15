package ProgrammingBatch;

public class Pattern6 {
    public static void main(String[] args) {
        int line=7;
        int star=4;
        for (int i=0;i<line;i++)
        {
            for (int j=0;j<star;j++)
            {
                System.out.print("*"+"\t");
            }
            System.out.println();
            if (i<=2)
            {
                star--;
            }
            else
            {
                star++;
            }
        }
    }
}
