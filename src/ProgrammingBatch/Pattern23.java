package ProgrammingBatch;
public class Pattern23 {
    public static void main(String[] args) {
        int line=7;
        int star=1;
        for ( int i=0; i<line;i++)
        {
            int ch=3;
            for (int j=0;j<star;j++)
            {
                System.out.print(ch--);
            }
            System.out.println();
            if (i<3)
            {
                star++;
                ch--;
            }
            else
            {
                star--;
            }
        }
    }
}
