package ProgrammingBatch;

public class Pattern26 {
    public static void main(String[] args) {
        int line=3;
        int star=3;
        for (int r=0;r<=line;r++)
        {
            for (int c=0;c<=star;c++)
            {
                System.out.print("*"+"\t");
            }
            System.out.println();
            star--;
        }
    }
}
