package Array;

import java.util.Scanner;

public class Floor {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the total no of floor");
        int floor= sc1.nextInt();
        System.out.println("Enter the flat no of each floor");
        int flat= sc1.nextInt();

        //Create 2D Array

        int[][]data=new int[floor][flat];
        System.out.println("Enter"+(floor*flat)+"flat NO");

        //Accept details

        for (int a=0;a<floor;a++)
        {
            for (int b=0; a<floor;b++)
                data[a][b]=sc1.nextInt();
        }
       for (int a=0;a<=floor;a++)
       {
           System.out.println("Floor no:"+a+1);
           System.out.println("================================================");

           for (int b=0; b<flat; b++)
           {
               System.out.println("Flat no:"+data[a][b]+"\t");
           }
           System.out.println();
           System.out.println("=======================================================");
       }
    }
}
