package Array;

public class BillCalculator {
    public static void calculateBill(double[] amounts) {
        //Store gst value
        double[] gstValues = gstCalculation(amounts);
        // Create new array Calculate total
        double[] totalAmounts = new double[amounts.length];
        // perform sum of amount and gst value
        for (int a = 0; a < amounts.length; a++) {
            totalAmounts[a] = amounts[a] + gstValues[a];
        }
        double totalBillAmt = 0.0;
        double totalGstAmt = 0.0;
        double totalFinalAmt = 0.0;

        //sum of Array elements
        for (int a = 0; a < amounts.length; a++) {
            totalBillAmt += amounts[a];
            totalGstAmt += gstValues[a];
            totalFinalAmt += totalAmounts[a];
        }
        //Present final outPut of user
        System.out.println("Bill.AMT\tGST.AMT\t  TOTAL");
        System.out.println("=======================================");
        for (int a = 0; a < amounts.length; a++) {
            System.out.println( amounts[a]+"\t"   +           gstValues[a]+"\t"           +        totalAmounts[a]);
        }
        System.out.println("=============================================");
        System.out.println(  totalBillAmt +"\t"     +            totalGstAmt +"\t"           +          totalFinalAmt);
    }
    public static double[] gstCalculation(double[] amounts)
    {
        //Create new array to store gst Amount
        double[] gstAmount=new double[amounts.length];
        for (int a=0;a<amounts.length;a++)
        {
            if (amounts[a]<500)
            {
                gstAmount[a]=amounts[a]+0.05;

            }
        else
            {
                gstAmount[a]=amounts[a]*0.1;
            }
           // return gst array to calculatorBill Method
           // return gstAmount;

        }
        return gstAmount;
    }
}
