package Array;

import java.util.Scanner;

public class ArrayDemo11 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("Enter the total number of bill");
        int count=sc1.nextInt();
        double[] amounts=new  double[count];
        System.out.println("Enter bill Amount");
        //Accept bill Amount
        for (int a=0;a<count;a++)
        {
            amounts[a]=sc1.nextDouble();
        }
            //call method from business class
        b1.calculateBill(amounts);
    }
}
