package ExceptionHandling;

public class ExceptionDemo2 {
    public static void main(String[] args) {
        System.out.println("program started");
        String str=null;
        try {                                        // try block
            System.out.println(str.toLowerCase());   // error line
        }
        catch (NullPointerException a)               // catch block
        {
            System.out.println(a);
        }
        System.out.println("end Program");
    }
}
