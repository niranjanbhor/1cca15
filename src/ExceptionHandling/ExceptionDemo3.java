package ExceptionHandling;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo3 {
    public static void main(String[] args) {
        System.out.println("Program Started");
        Scanner sc2=new Scanner(System.in);
        try {
            System.out.println("Enter the First no");
            int no1= sc2.nextInt();
            System.out.println("Enter the no second");
            int no2= sc2.nextInt();
            int result=no1/no2;
            System.out.println(result);
        }
        catch (ArithmeticException a)
        {
            System.out.println(a);
        }
        catch (InputMismatchException n)
        {
            System.out.println(n);
        }
        System.out.println("Program end");
    }
}
