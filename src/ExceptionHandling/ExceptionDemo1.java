package ExceptionHandling;

public class ExceptionDemo1 {
    public static void main(String[] args) {

         System.out.println("Program Started");
          String s1="123abc";
          try {                                    // try block
              int n1=Integer.parseInt(s1);         // error line
              System.out.println(n1);
          }
          catch (NumberFormatException a)          // catch block
          {
              System.out.println(a);
          }

        System.out.println("Program Ended");

    }
}
