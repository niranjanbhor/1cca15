package JavaAPI;

public class ParsingDemo {
    public static void main(String[] args) {
        String s1="45";
        String s2="23";
        System.out.println(s1+s2);  //Concat the two String

        int no1=Integer.parseInt(s1);
        int no2=Integer.parseInt(s2);
        System.out.println(no1+no2);  //Convert the String the int

        double d1=34.56;
        double d2=40.40;
        System.out.println(d1+d2);
        String str1=Double.toString(d1);      //Using toString method convert the Double value into the String
        String str2=Double.toString(d2);
        String str3=Double.toString(d1+d2);


        System.out.println(str2+str2);       // convert the double into String then concat
    }
}
