package JavaAPI;

public class HashcodeDemo {
    public static void main(String[] args) {
        String s1=new String("JAVA");
        String s2=new String("JAVA");
        String s3=new String("SQL");


        System.out.println(s1==s2);    //check the ip address
        System.out.println(s1==s3);    // check the ip Address

        System.out.println(s1.hashCode()==s2.hashCode());
        System.out.println(s1.hashCode()==s3.hashCode());

        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());
    }
}
