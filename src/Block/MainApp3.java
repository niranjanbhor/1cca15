package Block;


public class MainApp3 {

    static  int a=12;
    int b=45;

    static
    {
        System.out.println("Static Block");
    }
    {
        System.out.println("Non static Block");
    }

    public static void main(String[] args) {
        System.out.println("Main Method");
        //System.out.println("Value od A:"+MainApp3.a);
        //MainApp3 m1=new MainApp3();
        MainApp3 m1=new MainApp3();
        System.out.println("Value od A:"+MainApp3.a);
        MainApp3 m2=new MainApp3();
        System.out.println("Value of B:"+m2.b);
    }
}
