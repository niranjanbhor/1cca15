package Gaurav;

public class Pattern7
{
    public static void main(String[] args)
    {
        int line=11;
        int star=11;

        for (int i=0; i<line; i++)
        {
            for (int j=0; j<star; j++)
            {
                if (i==5||j==5)
                {
                    System.out.print("*"+"\t");
                }
                else
                {
                    System.out.print(" "+"\t");
                }
            }
            System.out.println();
        }
    }
}
