package ProgramingPattern;

public class Pattern8
{
    public static void main(String[] args)
    {
        int line=5;
        int star=5;

        for (int i=0; i<line; i++)
        {
            for (int j=0; j<star; j++)
            {
                if (j==0||i==0||i+j==4||i==4||j==4||i==j)
                {
                    System.out.print("*"+"\t");
                }
                else
                {
                    System.out.print(" "+"\t");
                }
            }
            System.out.println();
        }
    }
}
