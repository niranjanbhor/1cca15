package Inheritance;

public class MainApp3 {
    public static void main(String[] args) {
        PermanentEmployee p1=new PermanentEmployee();
        p1.getInfo(101,30000);
        p1.getDesignation("Developer");
        System.out.println("================================================");
        ContractEmployee c1=new ContractEmployee();
        c1.getInfo(201,3000);
        c1.getContractDetails(24);
    }
}
