package Test;
//write the java program to accept an element for the user and find out the position of that element inside the given array
import java.util.Arrays;

public class Que2 {

    public static void main(String[] args) {
        int[] arr1 = {10, 20, 30};
        int[] arr2 = {40, 50, 60};

        int a = arr1.length;
        int b = arr2.length;
        int[] result = new int[a + b];

        System.arraycopy(arr1, 0, result, 0, a);
        System.arraycopy(arr2, 0, result, a, b);

        System.out.println(Arrays.toString(result));
    }
}