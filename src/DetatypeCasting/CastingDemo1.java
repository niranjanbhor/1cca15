package DetatypeCasting;

public class CastingDemo1 {
    public static void main(String[] args) {
        int a=23;
        double b=234.56;
        System.out.println("A:"+a + "B:"+b);  // a=23  ,b=234.56

        int x= (int) 12.45;  //Narrowing  explicit
        double y=345;        // winding   implicit
        System.out.println("A:"+ x +"B:"+y);  // x=12,y=345.0

    }
}
