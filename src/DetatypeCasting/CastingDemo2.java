package DetatypeCasting;

public class CastingDemo2 {
    public static void main(String[] args) {
        char ch1='A';
        char ch2='B';

        int x1=ch1;                               //winding
        int x2=ch2;                               //Winding  // implicit
        System.out.println(x1+"\t\t"+x2);

        int x3=1234567895;
        char ch3= (char) x3;                    //Narrowing  //explicit
        System.out.println("cha3:"+ch3);

        double x4=123.45;
        char ch4= (char) x4;                    //Narrowing
        System.out.println("ch4:"+x4);
    }
}
