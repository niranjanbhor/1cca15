package DetatypeCasting;

public class CastingDemo3 {
    public static void main(String[] args) {
        short s1= (short) 1234545668;
        System.out.println(s1);

        long c1= (long)136853655556452l;
        int x1= (int) c1;
        System.out.println(x1);

    }
}
