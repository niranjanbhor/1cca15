package MethodOverloding;

import java.util.Scanner;

public class StudentMainApp {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Criteria");
        System.out.println("1:Select By Name");
        System.out.println("2:Select By ContactNo");
        int choice= sc1.nextInt();   // Because 1,2 is int

        if (choice==1)
        {
            System.out.println("Enter your Name");
            String name=sc1.next();
            s1.search(name);
        }
        else if (choice==2)
        {
            System.out.println("Enter your contact No");
            int contact= sc1.nextInt();
            s1.search(contact);
        }
       else
        {
            System.out.println("invalid choice");
        }
    }
}
