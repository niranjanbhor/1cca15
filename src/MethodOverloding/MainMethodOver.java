package MethodOverloding;

public class MainMethodOver {
    // Standard main method
    public static void main(String[] args) {
        System.out.println("Main Method");
        main(52);
        main('k');
    }
    // external method or non-Standard main method
    public static void main(int a)
    {
        System.out.println("A:"+a);
        System.out.println("Main Method 1");
    }
    // external method or non-Standard main method
    public static void main(char c)
    {
        System.out.println("c:"+c);
        System.out.println("Main method 2");
    }
}
