package MethodOverloding;

import java.util.Scanner;

public class EmpMainApp {
    public static void main(String[] args) {
        Emp e1=new Emp();
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter emp details");
        System.out.println("1:Enter the Gmail & pass");
        System.out.println("2:Enter the  contact & pass");
        int choice= sc1.nextInt();

        if (choice==1)
        {
            System.out.println("Enter the Gmail");
            String gmail= sc1.next();
            System.out.println("Enter the pass");
            int pass= sc1.nextInt();
            e1.login(gmail,pass);
        }
        else if (choice==2)
        {
            System.out.println("Enter the Contact no");
            long contact=sc1.nextLong();
            System.out.println("Enter the pass");
            int pass= sc1.nextInt();
            e1.login(contact,pass);
        }
        else
        {
            System.out.println("Invalid details");
        }
    }
}
