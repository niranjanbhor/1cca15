package MethodOverloding;

public class Emp {

       String gmail="niranjanbhor@gmail.com";
       int pass=1234;
       long contact=93707196;

       void login(String empGmail,int empPass)
       {
           if (empGmail.equals(gmail) && (pass==empPass))
           {
               System.out.println("login Successfully");
           }
          else
           {
               System.out.println("Invalid Details");
           }
       }

       void login(long empContact,int empPass)
       {
           if (contact==empContact && (pass==empPass))
           {
               System.out.println("login SuccessFully");
           }
           else
           {
               System.out.println("invalid details");
           }
       }
}
