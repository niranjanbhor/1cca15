package MethodOverriding;

import java.util.Scanner;

public class MainAppEcommerce {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the qty");
        int qty= sc1.nextInt();
        System.out.println("Enter the Price");
        double price= sc1.nextDouble();

        System.out.println("Select the Way of Parches");
        System.out.println("1:Flipkart \n2:Amazon");
        int choice= sc1.nextInt();

        if (choice==1)
        {
            Flipkart f1=new Flipkart();
            f1.sellProduct(qty,price);
        }
        else if (choice==2)
        {
            Amazon a1=new Amazon();
            a1.SellProduct(qty,price);
        }
    }
}
