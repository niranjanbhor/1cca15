package MethodOverriding;

import java.util.Scanner;

public class MainAppCinema {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter the qty od ticket");
        int qty= sc1.nextInt();
        System.out.println("Enter the price of ticket");
        double price= sc1.nextDouble();

        System.out.println("Select the cinema");
        System.out.println("1:Kgf\t2:Pushpa");
        int choice= sc1.nextInt();

        if (choice==1)
        {
            Kgf k1=new Kgf();
            k1.sellTicket(qty,price);
        }
        else if (choice==2)
        {
            Pushpa p1=new Pushpa();
            p1.sellTicket(qty,price);
        }

    }
}
