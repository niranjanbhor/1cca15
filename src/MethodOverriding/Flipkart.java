package MethodOverriding;

public class Flipkart {   // Use SameParameters
    void sellProduct(int qty,double price)
    {
        double total=qty*price;

        // 10% discount
        double finalAmt=total-total*0.1;
        System.out.println("FinalAmt:"+finalAmt);
    }
}
