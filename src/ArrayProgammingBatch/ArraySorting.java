package ArrayProgammingBatch;

import java.sql.Array;
import java.util.Arrays;

public class ArraySorting {
    public static void main(String[] args) {
        int [] a={1,7,6,9,2,8,4};
        System.out.println("Before sorting");
        System.out.println(Arrays.toString(a));

        System.out.println("After sorting");
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));

    }
}
