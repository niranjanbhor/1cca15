package SeriesProgramming;

public class Pattern1 {
    public static void main(String[] args) {
        int line=4;
        int star=1;
        for (int i=0;i<line;i++)
        {
            int ch=0;
            int ch1=1;
            for (int j=0;j<star;j++)
            {
                System.out.print(ch+" ");
                int a=ch+ch1;
                ch=ch1;
                ch1=a;

            }
            System.out.println();
            star+=2;
        }
    }
}
